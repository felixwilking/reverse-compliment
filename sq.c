#include <stdio.h>
#include <stdlib.h>

struct Layer;

struct Network;

typedef struct Node {
    double* weights;
    double bias;
    double value;
    struct Layer* layer;
} Node;

typedef struct Layer {
    Node** nodes;
    int length;
    struct Layer* previousLayer;
    struct Network* network;
} Layer;

typedef struct Network {
    struct Layer** layers;
    int* layerSizes;
    int length;
} Network;

Node* newNode(double bias, Layer* layer){
    Node* node = (Node*) malloc(sizeof(Node));
    node->weights = (double*) calloc(layer->previousLayer->length, sizeof(double));
    node->bias = bias;
    node->value = 0.0;
    node->layer = layer;
    return node;
}

void newLayer(Layer* layer, int length, Layer* previousLayer, Network* network){
    layer->nodes = (Node**) malloc(length * sizeof(Node*));
    layer->length = length;
    layer->previousLayer = previousLayer;
    layer->network = network;
    for (int i = 0; i < length; i++){
        layer->nodes[i] = newNode(1.0, layer);
    }
}

void initializeNetwork(Network* network){
    network->layers = (Layer**) calloc(network->length, sizeof(Layer*));
    network->layers[0] = (Layer*) malloc(sizeof(Layer));
    newLayer(network->layers[0], network->layerSizes[0], network->layers[0], network);
    for (int i = 1; i < network->length; i++){
        network->layers[i] = (Layer*) malloc(sizeof(Layer));
        newLayer(network->layers[i], network->layerSizes[i], network->layers[i - 1], network);
    }
}

Network* newNetwork(int* layerSizes, int length){
    Network* network = (Network*) malloc(sizeof(Layer*) * length + (sizeof(int) + 1) * length);
    network->length = length;
    network->layerSizes = layerSizes;
    initializeNetwork(network);
    return network;
}

void clearNetwork(Network* network, double* values){
    for (int i = 0; i < network->layers[0]->length; i++){
        network->layers[0]->nodes[i]->value = values[i];
    }
    for (int i = 1; i < network->length; i++){
        for (int j = 0; j < network->layers[i]->length; j++){
            network->layers[i]->nodes[j]->value = 0;
        }
    }
}

void runNetwork(Network* network){
    for (int i = 1; i < network->length; i++){
        for (int j = 0; j < network->layers[i]->length; j++){
            network->layers[i]->nodes[j]->value += network->layers[i]->nodes[j]->bias;
            for (int k = 0; k < network->layers[i - 1]->length; k++){
                network->layers[i]->nodes[j]->value += 
                network->layers[i]->previousLayer->nodes[k]->value * 
                (network->layers[i]->nodes[j]->weights[k] + 1);
            }
        }
    }
}

double* scoreNetwork(Network* network, double* answers){
    Layer* lastLayer = network->layers[(network->length) - 1];
    double* scores = (double*) malloc(lastLayer->length * sizeof(double));
    for (int i = 0; i < lastLayer->length; i++){
        scores[i] = (answers[i] - lastLayer->nodes[i]->value);
    }
    return scores;
}

void displayNetwork(Network* network, double* scores){
    printf("\n-----------------------------:>\n");
    for (int i = 0; i < network->length; i++){
        for (int j = 0; j < network->layers[i]->length; j++){
            printf("%f ", network->layers[i]->nodes[j]->value);
        }
        printf("\n");
    }
    printf("\n");
    for (int i = 0; i < network->layers[(network->length) - 1]->length; i++){
        printf("%f ", scores[i]);
    }
    printf("\n-----------------------------:>\n");
}

void backPropagateError(Network* network, double score, Layer* layer, int nodeIndex){
    if (layer == layer->previousLayer) return;
    double value;
    for (int i = 0; i < layer->previousLayer->length; i++){
        // printf("%f %f %f %f\n", score, layer->previousLayer->length, layer->nodes[nodeIndex]->value, layer->nodes[nodeIndex]->weights[i]);
        value = (layer->nodes[nodeIndex]->weights[i] * layer->previousLayer->nodes[i]->value + 1) / score;
        backPropagateError(network, value, layer->previousLayer, i);
        // printf("%f ", value);
        layer->nodes[nodeIndex]->weights[i] = value;
        // layer->nodes[nodeIndex]->weights[i] *= layer->nodes[nodeIndex]->weights[i];
    }
    // network->layers[layerIndex]->nodes[nodeIndex]->value = 1;
    // double multiplier;
    // for (int i = 0; i < network->layers[layerIndex + 1]->length; i++){
    //     if (nodeIndex == 0) 
    //         multiplier = backPropagateError(network, scores, layerIndex + 1, i);
    //     else
    //         multiplier = network->layers[layerIndex + 1]->nodes[i]->value;
    //         // printf("%d %d %d\n", multiplier, network->layers[layerIndex + 1]->nodes[i]->weights[nodeIndex], network->layers[layerIndex + 1]->length);
    //     network->layers[layerIndex]->nodes[nodeIndex]->value += 
    //     (multiplier * network->layers[layerIndex + 1]->nodes[i]->weights[nodeIndex]) / network->layers[layerIndex + 1]->length;
    // }
    // return network->layers[layerIndex]->nodes[nodeIndex]->value;
}

void adjustWeights(Network* network){
    double v = 0.0;
    for (int layerIndex = (network->length - 1); layerIndex >= 0; layerIndex--){
        for (int nodeIndex = 0; nodeIndex < network->layers[layerIndex]->length; nodeIndex++){
            for (int weightIndex = 0; weightIndex < network->layers[layerIndex]->previousLayer->length; weightIndex++){
                // printf("%d, %d\n", network->layers[layerIndex]->previousLayer->nodes[weightIndex]->value, 1000 * (network->layers[layerIndex]->previousLayer->nodes[weightIndex]->value == v));
                // v = network->layers[layerIndex]->previousLayer->nodes[weightIndex]->value;
                network->layers[layerIndex]->nodes[nodeIndex]->weights[weightIndex] *= network->layers[layerIndex]->previousLayer->nodes[weightIndex]->value;
            }
        }
    }
}

int main(){
    int layerSizes[] = {5, 20, 5, 5, 5, 5};
    int length = sizeof(layerSizes) / sizeof(*layerSizes);
    Network* network = newNetwork(layerSizes, length);
    // double values[] =  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    // double answers[] = {1, 1, 1, 1, 1, 1, 1, 100, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    double values[] =  {1, 1, 1, 1, 1};
    double answers[] = {1, 1, 1, 5, 1};

    // network->layers[network->length - 1]->nodes[1]->weights[1] = -0.1;
    // network->layers[network->length - 2]->nodes[0]->weights[0] = -0.1;
    // network->layers[network->length - 3]->nodes[0]->weights[0] = -0.2;
    for (int i = 0; i < 2; i++){
        clearNetwork(network, values);

        runNetwork(network);

        double* scores = scoreNetwork(network, answers);
        displayNetwork(network, scores);

        for (int j = 0; j < network->layers[network->length - 1]->length; j++){
            backPropagateError(network, scores[j], network->layers[network->length - 1], j);
        }
        // // printf("ididaijdw\n");
        // // displayNetwork(network, scores);
        // adjustWeights(network);
    }
}