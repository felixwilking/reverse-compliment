#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printArr(unsigned char* arr, int len){
    for (int i = 0; i < len; i++){
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main(int argc, char** argv){
    int s = atoi(argv[1]);
    unsigned char* arr = (unsigned char*) malloc(sizeof(unsigned char*) * s);
    memset(arr, 0x01, s);
    memset(arr + s - s / 3, 0x02, s / 3);
    printf("\n");
    printArr(arr, s);
    unsigned char temp;
    for (int i = 0; i < (s / 2); i++){
        temp = ~arr[i] & 0x03;
        arr[i] = ~arr[s - i - 1] & 0x03;
        arr[s - i - 1] = temp;
    }
    printArr(arr, s);
}