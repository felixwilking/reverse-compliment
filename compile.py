import os
from sys import argv
import time

os.system("gcc " + argv[1] + " -o " + argv[1].split(".")[0])
print("\n\n---------------------------------->>\n")

avg = 0
trials = 100000
for _ in range(trials):
    t = time.time()
    os.system("./" + argv[1].split(".")[0] + " " + " ".join(argv[2:]))
    avg += time.time() - t

print("\n\n---------------------------------->>\n")
print(avg / trials, "milliseconds")