#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct Node {
    struct Node** nodes;
    double* weights;
    double* errors;
    double value;
    int len;
} Node;

Node* newNode(int len, Node** nodes){
    Node* node = (Node*) malloc(sizeof(Node));
    node->nodes = nodes;
    node->weights = (double*) malloc(len * sizeof(double));
    node->errors = (double*) malloc(((len) ? (len) : (1)) * sizeof(double));
    for (int i = 0; i < len; i++){
        node->weights[i] = 1;
        node->errors[i] = 1;
    }
    node->value = 0;
    node->len = len;
    return node;
}

void newNetwork(Node* head, int* layerLengths, int layerCount){
    if (layerCount >= 2){
        Node** nodesNext = (Node**) malloc(layerLengths[1] * sizeof(Node*));
        for (int i = 0; i < layerLengths[0]; i++){
            head->nodes[i] = newNode(layerLengths[1], nodesNext);
        }
        newNetwork(newNode(layerLengths[1], nodesNext), layerLengths + 1, layerCount - 1);
    }
    else
    {
        for (int i = 0; i < layerLengths[0]; i++){
            head->nodes[i] = newNode(0, head->nodes);
        }
    }
}

void runNetwork(Node* head){
    for (int i = 0; i < head->len; i++){
        for (int j = 0; j < head->nodes[i]->len; j++){
            head->nodes[i]->nodes[j]->value += head->nodes[i]->weights[j] * head->nodes[i]->value;
        }
    }
    if (head->len != 0){
        runNetwork(head->nodes[0]);
    }
}

void displayNetwork(Node* head, int sigfigs){
    printf("\n<");
    for (int i = 0; i < head->len; i++){
        printf("%d: ", (int) (head->nodes[i]->value * sigfigs));
        for (int j = 0; j < head->nodes[i]->len; j++){
            printf("[%d, {%d}]\t", (int) (head->nodes[i]->weights[j] * sigfigs), (int) (head->nodes[i]->errors[j] * sigfigs));
        }
        if (head->nodes[i]->len == 0) printf("{%d}", (int) (head->nodes[i]->errors[0] * sigfigs));
        printf("\t");
    }
    printf(">\n");
    if (head->len != 0){
        displayNetwork(head->nodes[0], sigfigs);
    }
    printf("\n");
}

double* scoreNetwork(Node* head, double* answers, int answerCount){
    if (head->len != 0){
        return scoreNetwork(head->nodes[0], answers, answerCount);
    }
    double* scores = (double*) malloc(answerCount * sizeof(double));
    for (int i = 0; i < answerCount; i++){
        scores[i] = head->nodes[i]->errors[0] = answers[i] - head->nodes[i]->value;
    }
    return scores;
}

double sumLayerWeights(Node* head, int weightIndex){
    double sum = 0;
    for (int i = 0; i < head->len; i++){
        sum += head->nodes[i]->weights[weightIndex];
    }
    return (sum) ? (sum) : (1);
}

double getErrors(Node* head, int index){
    double n, sum = 0;
    double e, w, s;
    for (int i = 0; i < head->nodes[index]->len; i++){
        e = getErrors(head->nodes[index], i);
        w = head->nodes[index]->weights[i];
        s = sumLayerWeights(head, i);
        n = e * w / s;
        n = (n) ? (n) : (1);
        // printf("%d, %d, %f, %f, %f, %f\n", index, i, e, w, s, n);
        head->nodes[index]->errors[i] = n;
        head->nodes[index]->weights[i] *= n;
        sum += n;
    }
    if (index < (head->len - 1)) getErrors(head, index + 1);
    return head->errors[index];
}

int main(int argc, char** argv){
    int layerLengths[] = {3, 3, 3, 3, 3};
    int layerCount = sizeof(layerLengths) / sizeof(*layerLengths);
    Node** nodes = (Node**) malloc(layerLengths[0] * sizeof(Node*));
    Node* head = newNode(layerLengths[0], nodes);
    newNetwork(head, layerLengths, layerCount);
    int values[] = {1, 1, 10};
    assert((sizeof(values) / sizeof(*values)) == head->len);
    for (int i = 0; i < head->len; i++){
        head->nodes[i]->value = values[i];
    }
    assert((sizeof(values) / sizeof(*values)) == head->len);
    for (int i = 0; i < head->nodes[0]->len; i++){
        head->nodes[0]->nodes[i]->value = values[i];
    }
    displayNetwork(head, 100);
    runNetwork(head);
    displayNetwork(head, 100);
    double answers[] = {1, 1, 1};
    assert((sizeof(answers) / sizeof(*answers)) == layerLengths[layerCount - 1]);
    scoreNetwork(head, answers, layerLengths[layerCount - 1]);
    getErrors(head, 0);
    displayNetwork(head, 100);
    printf("%f ", head->nodes[0]->weights[0]);
    runNetwork(head);
    assert((sizeof(values) / sizeof(*values)) == head->len);
    for (int i = 0; i < head->len; i++){
        head->nodes[i]->value = values[i];
    }
    displayNetwork(head, 100);
}